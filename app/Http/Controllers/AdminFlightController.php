<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Flight;
use Illuminate\Support\Facades\Redirect;

class AdminFlightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model_flight = new Flight();

        $flights = $model_flight->getFlights(null, null);

        return view('admin.flight.list',['flights' => $flights]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model_flight = new Flight();

        $cities = $model_flight->getCities();
        $buses = $model_flight->getBuses();

        return view('admin.flight.add', ['buses' => $buses,
                                              'cities' => $cities]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $busNameId = $request->input('busNameId');
        $firstCityId = $request->input('firstCityId');
        $secondCityId = $request->input('secondCityId');
        $dispatchTime = $request->input('dispatchTime');
        $arrivalTime = $request->input('arrivalTime');


        $flight = new Flight();
        $flight->Bus_id = $busNameId;
        $flight->First_City_id = $firstCityId;
        $flight->Second_City_id = $secondCityId;
        $flight->Dispatch_time = $dispatchTime;
        $flight->Arrival_time = $arrivalTime;

        $flight->save();

        return Redirect::to('/admin/flights');
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model_flight = new Flight();

        $cities = $model_flight->getCities();
        $buses = $model_flight->getBuses();


        $flight = Flight::where('Flight_id', $id)->first();

        return view('admin.flight.edit', [
            'flight' => $flight,
            'buses' => $buses,
            'cities' => $cities
            ]);





    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

          $flight = Flight::where('Flight_id', $id)->first();

        $flight->Bus_id = $request->input('busNameId');
        $flight->First_City_id = $request->input('firstCityId');
        $flight->Second_City_id = $request->input('secondCityId');
        $flight->Dispatch_time = $request->input('dispatchTime');
        $flight->Arrival_time = $request->input('arrivalTime');

        $flight->save();
        return Redirect::to('/admin/flights');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Flight::destroy($id);
        return Redirect::to('/admin/flights');
    }
}
