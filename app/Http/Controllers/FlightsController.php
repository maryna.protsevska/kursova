<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\Flight;

class FlightsController extends Controller
{
    public function list(Request $request)
    {
        $sort = $request->input('sort', null);
        $busName = $request->input('busName', null);
        $model_flights = new Flight();
        $flights = $model_flights->getFlights($busName, $sort);
        $sorts = $model_flights->getSorts();
        return view('app.flights.list', [
                'flights' => $flights,
                'busName_selected' => $busName,
                'type_sort' => $sorts,
                'sort_selected' => $sort]
        );

        //return view('app.flights.list');
    }

    public function flight($id)
    {
        $model_flights = new Flight();
        $flight = $model_flights->getFlightByID($id);
        return view('app.flights.flight')->with('flight', $flight);

    }

}
