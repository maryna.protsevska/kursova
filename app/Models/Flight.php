<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Flight extends Model
{
    public const SORT_NO_SORT = 1;
    public const SORT_BY_DATE = 2;
    public const SORT_BY_DESTINATION = 3;

        use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Flight';


    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'Flight_id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function getSorts()
    {
        $sorts = DB::table('Type_sort')
            ->select('Type_sort.Type_sort_id', 'Type_sort.Title as Sort_title')
            ->get();

        return $sorts;
    }

    public function getBuses()
    {

        $buses = DB::table('Bus')
                      ->get();

        return $buses;
    }

      public function getCities()
    {
        $cities = DB::table('City')
                      ->get();

        return $cities;
    }

      public function getCountSeatsByBusId($BusId)
    {

        $countseats = DB::table('Bus')
            ->select('Bus.Count_Seats')
            ->where('Bus.Bus_id', '=', $BusId)
            ->get();

        return $countseats;
    }


    public function getFlights($numBus,$sort){

        $query = DB::table('Flight')
            ->select('Flight.Flight_id', 'Bus.Bus_Name', 'Bus.Count_Seats',
                'City1.Title as dis_Title',
                'City2.Title as arr_Title', 'Flight.Dispatch_time', 'Flight.Arrival_time')
            ->join('Bus', 'Bus.Bus_id', '=', 'Flight.Bus_id')
            ->join('City as City1', 'City1.City_id', '=', 'Flight.First_City_id')
            ->join('City as City2', 'City2.City_id', '=', 'Flight.Second_City_id');


        if($numBus)
            $query->where('Bus.Bus_Name', '=', $numBus);



        switch ($sort)
        {
            case $this::SORT_NO_SORT:
            {$flights = $query->orderBy('Flight.Flight_id')->get();
                break;}
            case $this::SORT_BY_DATE:
            {$flights = $query->orderBy('Flight.Dispatch_time', 'desc')->get();
                break;}
            case $this::SORT_BY_DESTINATION:
            {$flights = $query->orderBy('City2.Title')->get();
                break;}
            default:
                $flights = $query->get();

        }


        return $flights;
    }


    public function getFlightByID($id){

        if(!$id) return null;

        $flight = DB::table('Flight')
            ->select('Flight.Flight_id', 'Bus.Bus_Name', 'Bus.Count_Seats',
                'Flight.Count_Seats_Now', 'City1.Title as dis_Title',
                'City2.Title as arr_Title', 'Flight.Dispatch_time', 'Flight.Arrival_time')
            ->join('Bus', 'Bus.Bus_id', '=', 'Flight.Bus_id')
            ->join('City as City1', 'City1.City_id', '=', 'Flight.First_City_id')
            ->join('City as City2', 'City2.City_id', '=', 'Flight.Second_City_id')
            ->where('Flight.Flight_id', '=', $id)
            ->get()->first();
        return $flight;
    }




}

