@extends('admin.layout')
<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
@section('content')
    <h2>Додати рейс</h2>
    <form action="/admin/flights" method="POST">
        {{ csrf_field() }}

        <label>Назва автобусу</label>
                    <select name="busNameId" >
                         @foreach($buses as $bus)
                            <option value="{{ $bus->Bus_id}}">
                                {{ $bus->Bus_Name }}
                            </option>
                        @endforeach
                    </select>
                    <br/><br/>

                    <label>Від</label>
                        <select name="firstCityId">
                             @foreach($cities as $city)
                                <option value="{{ $city->City_id}}">
                                    {{ $city->Title }}
                                </option>
                            @endforeach
                        </select>
                         <br/><br/>
                <label>До</label>
                    <select name="secondCityId">
                          @foreach($cities as $city)
                            <option value="{{ $city->City_id }}"
                                {{ ( $city->City_id != 1) ? 'selected' : '' }}>
                                {{ $city->Title }}
                            </option>
                        @endforeach
                    </select>
                  <br/><br/>
                <label>Дата відправки</label>
                <input type="date" name="dispatchTime" >
                 <br/><br/>

                <label>Дата прибуття</label>
               <input type="date" name="arrivalTime" >
                <br/><br/>



        <input type="submit" value="Зберегти">
    </form>
@endsection
