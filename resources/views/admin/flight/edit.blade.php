@extends('admin.layout')
<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
@section('content')
    <h2>Редагування рейсу {{ $flight->Flight_id }}</h2>
    <form action="/admin/flights/{{ $flight->Flight_id }}" method="POST">
        {{ method_field('PUT') }}
        {{ csrf_field() }}

        <label>Назва автобусу</label>
        <select name="busNameId">
             @foreach($buses as $bus)
                <option value="{{ $bus->Bus_id}}"
                    {{ ( $bus->Bus_id == $flight->Bus_id) ? 'selected' : '' }}>
                    {{ $bus->Bus_Name }}
                </option>
            @endforeach
        </select>
        <br/><br/>

        <label>Від</label>
        <select name="firstCityId">
              @foreach($cities as $city)
                <option value="{{ $city->City_id}}"
                    {{ ( $city->City_id == $flight->First_City_id) ? 'selected' : '' }}>
                    {{ $city->Title }}
                </option>
            @endforeach
        </select>
        <br/><br/>
        <label>До</label>
        <select name="secondCityId">
            @foreach($cities as $city)
                <option value="{{ $city->City_id }}"
                    {{ ( $city->City_id == $flight->Second_City_id) ? 'selected' : '' }}>
                    {{ $city->Title }}
                </option>
            @endforeach
        </select>
        <br/><br/>
        <label>Дата відправки</label>
        <input type="date" name="dispatchTime" value="{{ $flight->Dispatch_time }}">
        <br/><br/>

        <label>Дата прибуття</label>
        <input type="date" name="arrivalTime"  value="{{ $flight->Arrival_time }}">
        <br/><br/>



        <input type="submit" value="Зберегти">
    </form>
@endsection
