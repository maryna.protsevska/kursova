@extends('admin.layout')
@section('content')
    <h2>Список рейсів</h2>
    <table>
            <th>Номер рейсу</th>
            <th>Назва автобусу</th>
            <th>Кількість місць</th>
            <th>Від</th>
            <th>До</th>
            <th>Дата відправки</th>
            <th>Дата прибуття</th>
            <th>Редагування</th>
            @foreach ($flights as $flight)
                <tr>
                    <td>
                        <a href="/flights/{{ $flight->Flight_id }}">

                            {{ $flight->Flight_id}}
                        </a>
                    </td>
                    <td>{{ $flight->Bus_Name }}</td>
                    <td>{{ $flight->Count_Seats}}</td>
                    <td>{{ $flight->dis_Title }}</td>
                    <td>{{ $flight->arr_Title }}</td>
                    <td>{{ $flight->Dispatch_time }}</td>
                    <td>{{ $flight->Arrival_time }}</td>

                    <td>
                        <form style="float:right; padding: 0 15px;"
                              action="/admin/flights/{{ $flight->Flight_id}}"method="POST">
                            {{ method_field('DELETE') }}

                            {{ csrf_field() }}
                            <button class="btn btn-danger">Видалити </button>

                        </form>
                            <form style="float:right; padding: 0 15px;"
                              action="/admin/flights/{{ $flight->Flight_id}}/edit"method="GET">
                            {{ csrf_field() }}
                            <button class="btn btn-danger">Змінити</button>

                            </form>

                    </td>
                </tr>
            @endforeach
        </table>
    <span style="width: 100%; text-align: right"><a href="/admin/flights/create"> <i style="font-size: 45px; float: right; margin:10px 50px" class="fas fa-plus-circle"></i></a></span>
@endsection
