<html>
<head>
    <title>Інформаційна система "Готель"</title>
    <link href="/css/app.css" rel="stylesheet">
    <script src="https://kit.fontawesome.com/dff063ab19.js" crossorigin="anonymous"></script>

    <style>
        body{
            background: linear-gradient(45deg, #EECFBA, #C5DDE8);

        }
        table{
            width:95%;
            margin: auto;
        }
        th, td{
            margin-top: 15px;
            padding: 10px;
            text-align: center;
        }
        td{
            text-align:center;
        }
        th,tr:nth-child(even) {
            background: rgba(255,255,255,0.1);
            box-shadow: 0 25px 45px rgba(0,0,0,0.1);
            border: 1px solid rgba(255,255,255,0.1);
            border-right: 1px solid rgba(255,255,255,0.1);
            border-bottom: 1px solid rgba(255,255,255,0.1);
            border-radius: 10px;
        }
        .btn-danger{
            background: rgba(160, 210, 235,1);
            color: #ffffff;
            box-shadow: 0 25px 45px rgba(0,0,0,0.1);
            border: 1px solid rgba(252, 3, 3,0.1);
            border-right: 1px solid rgba(252, 3, 3,0.1);
            border-bottom: 1px solid rgba(252, 3, 3,0.1);
            border-radius: 10px;
        }
        select{
            outline: none;
            background: rgba(160, 210, 235,1);
            border-radius: 10px !important;
            border: none;
            height: 35px;
            padding: 0 0 0 .5em;
            color: #fff;
            font-size: 1em;
            letter-spacing: .8px;
            cursor: pointer;
        }
    </style>
</head>
<body>


<h1>Ви зайшли як адміністратор</h1>

<div>
    @yield('content')
</div>
</body>
