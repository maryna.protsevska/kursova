@extends('app.layout')
@section('page_title')
    <b>Інформація про рейс {{ $flight->Flight_id }}</b>
@endsection
@section('content')
    <table>
        <tr>
            <td style="border-right:1px solid #95999c"><b>Назва автобусу</b></td><td>{{ $flight->Bus_Name }}</td>
        </tr>
        <tr>
            <td style="border-right:1px solid #95999c"><b>Кількість місць</b></td><td>{{ $flight->Count_Seats}}</td>
        </tr>
         <tr>
            <td style="border-right:1px solid #95999c"><b>Від</b></td>  <td>{{ $flight->dis_Title }}</td>
        </tr>
        <tr>
            <td style="border-right:1px solid #95999c"><b>До</b></td><td>{{ $flight->arr_Title }}</td>
        </tr>
        <tr>
            <td style="border-right:1px solid #95999c"><b>Дата відправки</b></td><td>{{ $flight->Dispatch_time }}</td>
        </tr>
        <tr>
            <td style="border-right:1px solid #95999c"><b>Дата прибуття</b></td> <td>{{ $flight->Arrival_time }}</td>
        </tr>
    </table>



    <a href="/">Дивитися всі рейси</a>

@endsection
