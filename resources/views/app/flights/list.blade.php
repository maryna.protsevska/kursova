@extends('app.layout')
@section('page_title')
    <h4 style="text-align: center; margin-top: 10px;font-style: italic; font-size: 24px">Список рейсів</h4>
@endsection
@section('content')
    <form method="get" action="/">
        <select class="form-select form-select-lg mb-3" name="sort">
                 @foreach( $type_sort as $sort)
                    <option value="{{ $sort->Type_sort_id }}"
                        {{ ( $sort->Type_sort_id == $sort_selected ) ? 'selected' :  '' }}>
                        {{ $sort->Sort_title }}
                    </option>
                @endforeach
            </select>

        <select  class="form-select form-select-lg mb-3" aria-label=".form-select-lg example" name="busName">
            <option value="null">всі автобуси</option>
            @foreach($flights as $flight)
                <option value="{{ $flight->Bus_Name }}"
                    {{ ( $flight->Bus_Name == $busName_selected) ? 'selected' : '' }}>
                    {{ $flight->Bus_Name }}
                </option>
            @endforeach
        </select>

        <input type="submit" class="btn btn-danger" value="Знайти" />
    </form>
    <table class="table">
        <thead>
        <tr>
        <th scope="col">Номер рейсу</th>
        <th>Назва автобусу</th>
        <th>Кількість місць</th>
        <th>Від</th>
        <th>До</th>
        <th>Дата відправки</th>
        <th>Дата прибуття</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($flights as $flight)
            <tr>
                <td>
                    <a href="/flights/{{ $flight->Flight_id }}">

                        {{ $flight->Flight_id}}
                    </a>
                </td>
                <td>{{ $flight->Bus_Name }}</td>
                <td>{{ $flight->Count_Seats}}</td>
                <td>{{ $flight->dis_Title }}</td>
                <td>{{ $flight->arr_Title }}</td>
                <td>{{ $flight->Dispatch_time }}</td>
                <td>{{ $flight->Arrival_time }}</td>
            </tr>
        @endforeach
    </table>
    <a href="/home" class="btn btn-danger">Увійти</a>
  @endsection
