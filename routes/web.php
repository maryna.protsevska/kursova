<?php
use App\Http\Controllers\FlightsController;
use App\Http\Controllers\AdminFlightController;
use Illuminate\Support\Facades\Route;



Route::get('/', [FlightsController::class, 'list']);
Route::get('flights/{flight_number}', [FlightsController::class, 'flight']);


Route::resource('admin/flights', AdminFlightController::class);





Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

